import os

import requests
import json


SERVER_HOST = os.environ.get('SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('SERVER_PORT', 80))
URL = f'http://{SERVER_HOST}:{SERVER_PORT}/'


def test_post_balance():

    resp = requests.post(URL, json={'id':1, 'sum':1000})
    assert resp.status_code == 200

def test_get_balance():

    resp = requests.post(URL, json={'id':2, 'sum':2000})
    assert resp.status_code == 200

    resp = requests.get(URL, json={'id':2})    
    assert resp.status_code == 200
    assert resp.content == b'{"id":2,"balance":2000}\n'

def test_delete_balance():

    resp = requests.post(URL, json={'id':3, 'sum':3000})
    assert resp.status_code == 200

    resp = requests.get(URL, json={'id':3})    
    assert resp.status_code == 200
    assert resp.content == b'{"id":3,"balance":3000}\n'

    resp = requests.delete(URL, json={'id':3})
    assert resp.status_code == 200

    resp = requests.get(URL, json={'id':3})
    print(resp.json)
    assert resp.status_code == 200
    assert resp.content == b'{"id":3,"balance":0}\n'

def test_clean():
    requests.delete(URL, json={'id':1})
    requests.delete(URL, json={'id':2})
    requests.delete(URL, json={'id':3})
